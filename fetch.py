#!/usr/bin/env python

# fetch.py
# Copyright © 2021 Ruben Di Battista
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY yourname ''AS IS'' AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL yourname BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# The views and conclusions contained in the software and documentation
# are those of the authors and should not be interpreted as representing
# official policies, either expressed or implied, of yourname.
""" A script used to fetch macOS old versions """
import click
import requests

from pathlib import Path
from tqdm import tqdm

oses = {
    "10.6": "https://updates.cdn-apple.com/2019/cert/041-87855-20191017-e9f288dc-eec9-49e5-998e-f828214d633d/MacOSXUpd10.6.8.dmg",
    "10.10": "http://updates-http.cdn-apple.com/2019/cert/061-41343-20191023-02465f92-3ab5-4c92-bfe2-b725447a070d/InstallMacOSX.dmg",
    "10.11": "http://updates-http.cdn-apple.com/2019/cert/061-41424-20191024-218af9ec-cf50-4516-9011-228c78eda3d2/InstallMacOSX.dmg",
    "10.12": "http://updates-http.cdn-apple.com/2019/cert/061-39476-20191023-48f365f4-0015-4c41-9f44-39d3d2aca067/InstallOS.dmg",
}

CONTEXT_SETTINGS = dict(help_option_names=["-h", "--help"])


def do_list():
    for os in oses:
        click.echo(os)


def do_fetch(os_name, url):
    click.echo(f"Fetching macOS {os_name}")

    file_name = f"{os_name}.dmg"

    if not ((Path.cwd() / file_name).stat()):
        response = requests.get(url, stream=True)
        total_size_in_bytes = int(response.headers.get("content-length", 0))
        block_size = 1024  # 1 Kibibyte
        progress_bar = tqdm(
            total=total_size_in_bytes, unit="iB", unit_scale=True
        )

        with open(f"{os_name}.dmg", "wb") as file:
            for data in response.iter_content(block_size):
                progress_bar.update(len(data))
                file.write(data)
        progress_bar.close()
        if total_size_in_bytes != 0 and progress_bar.n != total_size_in_bytes:
            click.echo("ERROR, something went wrong")
    else:
        click.echo("File already present. Skipping download.")


@click.command(context_settings=CONTEXT_SETTINGS)
@click.option(
    "-l",
    "--list",
    "list_oses",
    is_flag=True,
    default=False,
    help="List available macOS versions",
)
@click.option("-o", "--os", help="Fetch a specific macOS")
@click.pass_context
def main(ctx, list_oses, os):
    """ Fetch macOS images """
    if list_oses:
        do_list()

    elif os:
        do_fetch(os, oses[os])
    else:
        click.echo(ctx.get_help())


if __name__ == "__main__":
    main()
